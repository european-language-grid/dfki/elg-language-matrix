FROM node:12-alpine
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm run build && npx serve -s build -l 3000
