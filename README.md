elg-language-matrix
===================

This is a small demo webapp which displays which gives an overview of the
Language Resources and Technologies available in the
[European Language Grid](https://live.european-language-grid.eu).

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Development
-----------

For local development, you will need to be running a local proxy to the ELG
Catalogue Search API. Follow the instructions in the README here:
https://gitlab.com/european-language-grid/dfki/local-elg-api

Now change to the `elg-language-matrix` directory and run:

    npm install

The next step is to set an environment variable so the app knows where to find
the (locally proxied) API. If you are using Bash or similar, run:

    export REACT_APP_SEARCH_API=http://localhost:5000/catalogue_backend/api/registry/search

If you are using Windows PowerShell, run:

    $env:REACT_APP_SEARCH_API="http://localhost:5000/catalogue_backend/api/registry/search"

Now start the app:

    npm start

This will run the app in development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

Dockerization
-------------

Assuming Docker is installed and running on your local machine, you can build a
Docker image as follows:

    docker build -t elg-language-matrix .

You can then run it as a container locally as follows:

    docker run --name elg-language-matrix --publish 4680:3000 --detach --env 'REACT_APP_SEARCH_API=http://localhost:5000/catalogue_backend/api/registry/search' elg-language-matrix

Test that it works by visiting [http://localhost:4680](http://localhost:4680).
