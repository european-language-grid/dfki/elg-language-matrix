import React from 'react'
import ReactDOM from 'react-dom'
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './index.css'

const SEARCH_API = process.env.REACT_APP_SEARCH_API ? process.env.REACT_APP_SEARCH_API : 'https://live.european-language-grid.eu/catalogue_backend/api/registry/search/'
const CATALOGUE = process.env.REACT_APP_CATALOGUE ? process.env.REACT_APP_CATALOGUE : 'https://live.european-language-grid.eu/catalogue/'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#299ba3',
    color: theme.palette.common.white,
  },
}))(TableCell);

class Matrix extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      languages_eu: [],
      languages_eu_other: [],
      languages_rest: [],
      machine_translation: new Map(),
      text_analytics: new Map(),
      speech: new Map(),
      resources: new Map(),
    }
  }

  async componentDidMount() {
    let search = await fetch(SEARCH_API + '?entity_type__term=LanguageResource')
    let json = await search.json()
    const languages_eu = json.facets._filter_language_eu.language_eu.buckets
        .map(b => b.key)
        .map(this.normalize_language)
    const languages_eu_other = json.facets._filter_language_eu_other.language_eu_other.buckets
        .map(b => b.key)
    const languages_rest = json.facets._filter_language_rest.language_rest.buckets
        .map(b => b.key)
    languages_eu.sort()
    languages_eu_other.sort()
    languages_rest.sort()
    this.setState({
      languages_eu,
      languages_eu_other,
      languages_rest,
    })
    const machine_translation = new Map()
    const text_analytics = new Map()
    const speech = new Map()
    const resources = new Map()
    this.count(json, machine_translation, text_analytics, speech, resources)
    while (json.next !== null) {
      search = await fetch(json.next)
      json = await search.json()
      this.count(json, machine_translation, text_analytics, speech, resources)
    }
    this.setState({
      machine_translation,
      text_analytics,
      speech,
      resources,
    })
  }

  normalize_language(language) {
    if (language === 'Modern Greek (1453-)') {
      return 'Greek'
    }
    return language
  }

  count(json, machine_translation, text_analytics, speech, resources) {
    for (const result of json.results) {
      if (result.resource_type === 'Tool/Service') {
        let mt = false
        let ta = false
        let sp = false
        for (const fun of result.functions) {
          if (fun === 'Machine Translation') {
            mt = true
          } else if (fun === 'Speech Recognition' || fun === 'Speech Synthesis') {
            sp = true
          } else {
            ta = true
          }
        }
        if (mt) {
          this.add(machine_translation, result.languages, result.id)
        }
        if (ta) {
          this.add(text_analytics, result.languages, result.id)
        }
        if (sp) {
          this.add(speech, result.languages, result.id)
        }
      } else {
        this.add(resources, result.languages, result.id)
      }
    }
  }

  add(map, languages, id) {
    for (let language of languages) {
      language = this.normalize_language(language)
      if (!map.has(language)) {
        map.set(language, [])
      }
      map.get(language).push(id)
    }
  }

  renderEntry(ids) {
    if (ids === undefined || ids.length === 0) {
      return ''
    }
    const queryParts = ids.map(id => `id=${id}`)
    const query = queryParts.join('&')
    const href = CATALOGUE + '#/?' + query
    return (
      <a href={href}>{ids.length}</a>
    )
  }

  render() {
    return (
      <div>
        <h3>Official EU Languages</h3>
        <TableContainer component={Paper}>
          <Table className="elg-language-matrix" size="small" aria-label="ELG language matrix">
            <TableHead>
              <TableRow>
                <StyledTableCell width="20%">Language</StyledTableCell>
                <StyledTableCell width="20%" align="center">Machine Translation</StyledTableCell>
                <StyledTableCell width="20%" align="center">Text Analytics</StyledTableCell>
                <StyledTableCell width="20%" align="center">Speech</StyledTableCell>
                <StyledTableCell width="20%" align="center">Resources</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.languages_eu.map(lang => {
                return (
                  <TableRow key={lang}>
                    <TableCell>{lang}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.machine_translation.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.text_analytics.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.speech.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.resources.get(lang))}</TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <h3>Other EU languages</h3>
        <TableContainer component={Paper}>
          <Table className="elg-language-matrix" size="small" aria-label="ELG language matrix">
            <TableHead>
              <TableRow>
                <StyledTableCell width="20%">Language</StyledTableCell>
                <StyledTableCell width="20%" align="center">Machine Translation</StyledTableCell>
                <StyledTableCell width="20%" align="center">Text Analytics</StyledTableCell>
                <StyledTableCell width="20%" align="center">Speech</StyledTableCell>
                <StyledTableCell width="20%" align="center">Resources</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.languages_eu_other.map(lang => {
                return (
                  <TableRow key={lang}>
                    <TableCell>{lang}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.machine_translation.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.text_analytics.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.speech.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.resources.get(lang))}</TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <h3>Other Languages</h3>
        <TableContainer component={Paper}>
          <Table className="elg-language-matrix" size="small" aria-label="ELG language matrix">
            <TableHead>
              <TableRow>
                <StyledTableCell width="20%">Language</StyledTableCell>
                <StyledTableCell width="20%" align="center">Machine Translation</StyledTableCell>
                <StyledTableCell width="20%" align="center">Text Analytics</StyledTableCell>
                <StyledTableCell width="20%" align="center">Speech</StyledTableCell>
                <StyledTableCell width="20%" align="center">Resources</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.languages_rest.map(lang => {
                return (
                  <TableRow key={lang}>
                    <TableCell>{lang}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.machine_translation.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.text_analytics.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.speech.get(lang))}</TableCell>
                    <TableCell align="center">{this.renderEntry(this.state.resources.get(lang))}</TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}

ReactDOM.render(
  <Matrix />,
  document.getElementById('root')
);